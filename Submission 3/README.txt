Link to unlisted youtube video:
https://youtu.be/_Z7FtZDrxCs

Link to the Concept Map:
https://lucid.app/lucidchart/3e1064a8-3abe-421d-8269-aeec62402ec2/edit?viewport_loc=-3368%2C-782%2C10334%2C4315%2C0_0&invitationId=inv_6a715adf-f5b2-4550-9dfa-94a65a7dbecc

Pdf: https://drive.google.com/file/d/1qRVT3zdSAtTccQ4lhrierEkbjBVuQIO7/view?usp=sharing

Name, Author, Edition, year and publisher of the Book:
Speech and Language Processing (3rd ed.draft)
Jurafsky
3rd edition
2014
Pearson

Link to the book:
https://web.stanford.edu/~jurafsky/slp3/

Link to the book chapter:
https://web.stanford.edu/~jurafsky/slp3/17.pdf

Group Number:
01

Group Members:
21166003 Ayesha Siddika
21166010 Shanuma Afrin Meghla
21366006 Raihana Tabassum
21366008 Jayed Mohammad Barek
21366027 Md. Rashedul Hasan Safa
